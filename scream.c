#include <ncurses.h>
#include <stdlib.h>
#include <unistd.h>

WINDOW *create_newwin(int height, int width, int starty, int startx);
void key_for_mode( int ch );
void backspace( void );

WINDOW * draw_win;

int main(int argv, char ** argc) 
{
	int ch, row, col, c_row, c_col = -1;
	initscr();
	noecho();
	cbreak();
	getmaxyx( stdscr, row, col );
	
	draw_win = create_newwin( row, col, 0, 0 );
	
	keypad( stdscr, TRUE );
	keypad( draw_win, TRUE );

	getyx( draw_win, c_row, c_col );

	while ( (ch = wgetch( draw_win )) ) {

		getyx( draw_win, c_row, c_col );

		switch (ch) {
			case KEY_UP:
				if ( c_row > 0 ) wmove( draw_win, c_row - 1, c_col );
				break;

			case KEY_DOWN:
				if ( c_row < row ) wmove( draw_win, c_row + 1, c_col );
				break;

			case KEY_LEFT:
				if ( c_col > 0 ) wmove( draw_win, c_row, c_col - 1 );
				break;

			case KEY_RIGHT:
				if ( c_col < col ) wmove( draw_win, c_row, c_col + 1 );
				break;

			default:
				key_for_mode( ch );
				break;

		}

	}

	return 0;


}

void key_for_mode( int ch ) {
	int r, c;
	getyx( draw_win, r, c );
	if (( ch > 65 ) && (ch < 90 ) ) {
		waddch( draw_win, 'A' );
	} else if ((ch > 97) && (ch < 122)) {
		waddch( draw_win, 'a' );
	} else if ( (ch == KEY_BACKSPACE) || ( ch == 127 ) ) {
			backspace();
	} else {
		waddch(draw_win, ch);
	}
}

void backspace( void ) {
	int r, c;
	getyx( draw_win,r, c );
	if ( c == 0 ) return;
	wmove( draw_win,  r, c - 1 );
	waddch( draw_win,  ' ' );
	wmove( draw_win, r, c - 1 );
}



WINDOW *create_newwin(int height, int width, int starty, int startx)
{	
	WINDOW *local_win;
	local_win = newwin(height, width, starty, startx);
	return local_win;
}


